package s5.cloud.enchere.controller.common;

import static s5.cloud.enchere.util.ControllerUtil.returnSuccess;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import s5.cloud.enchere.exception.CustomException;
import s5.cloud.enchere.model.HasId;
import s5.cloud.enchere.service.common.Service;
import s5.cloud.enchere.util.SuccessResponse;

/*
* How to use:
*   1- Create a controller class that extends this class
*   2- create a service that extends CrudService
*   3- Add @RequestMapping annotation to the class
* Then you are good for CRUD operations
* */

public class CrudController<E extends HasId, S extends Service<E>> {

    protected final S service;

    public CrudController(S service) {
        this.service = service;
    }

    @PostMapping("")
    public ResponseEntity<SuccessResponse> create(@RequestBody E obj) throws CustomException {
        return returnSuccess(service.create(obj), HttpStatus.CREATED);
    }

    @GetMapping("/{id}")
    public ResponseEntity<SuccessResponse> findById(@PathVariable("id") Integer id) {
        return returnSuccess(service.findById(id), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<SuccessResponse> delete(@PathVariable Integer id) {
        service.delete(id);
        return returnSuccess("", HttpStatus.NO_CONTENT);
    }

    @GetMapping("")
    public ResponseEntity<SuccessResponse> findAll() {
        return returnSuccess(service.findAll(), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<SuccessResponse> update(@PathVariable("id") Integer id, @RequestBody E obj) throws CustomException {
        obj.setId(id);
        return returnSuccess(service.update(obj), HttpStatus.OK);
    }

}
