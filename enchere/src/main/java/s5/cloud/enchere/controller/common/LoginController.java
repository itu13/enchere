package s5.cloud.enchere.controller.common;

import static s5.cloud.enchere.util.ControllerUtil.returnSuccess;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import s5.cloud.enchere.exception.LoginException;
import s5.cloud.enchere.model.LoginEntity;
import s5.cloud.enchere.repo.LoginRepo;
import s5.cloud.enchere.service.LoginService;
import s5.cloud.enchere.service.ServiceLogin;

public class LoginController <E extends LoginEntity, S extends ServiceLogin<E>> {

    protected S service;

    public LoginController(S service) {
        this.service = service;
    }

    @PostMapping("/login")
    public ResponseEntity<?> login(@RequestBody E entity) throws LoginException {
        return returnSuccess(service.login(entity), HttpStatus.ACCEPTED);
    }

}
