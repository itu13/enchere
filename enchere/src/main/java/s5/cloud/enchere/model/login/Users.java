package s5.cloud.enchere.model.login;

import java.util.Date;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.Getter;
import lombok.Setter;
import s5.cloud.enchere.model.HasId;
import s5.cloud.enchere.model.LoginEntity;

@Entity
@Setter
@Getter
public class Users extends HasId implements LoginEntity{
     @ManyToOne
     private RoleType role;
     private String firstname;
     private String lastname;
     private String email;
     private Date bithdate;
     private String phoneNumber;
     private String joinedDate;
     private String password;
     @Override
     public String getPassword() {
          return password;
     }

     @Override
     public String getEmail() {
          return email;
     }
     
}
