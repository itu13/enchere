CREATE SCHEMA "public";

CREATE SEQUENCE account_id_seq START WITH 1;

CREATE SEQUENCE auction_id_seq START WITH 1;

CREATE SEQUENCE auction_operation_id_seq START WITH 1;

CREATE SEQUENCE auction_winner_id_seq START WITH 1;

CREATE SEQUENCE category_id_seq START WITH 1;

CREATE SEQUENCE role_type_id_seq START WITH 1;

CREATE SEQUENCE settings_id_seq START WITH 1;

CREATE SEQUENCE token_id_seq START WITH 1;

CREATE SEQUENCE users_id_seq START WITH 1;
CREATE SEQUENCE movement_type_id_seq START WITH 1;

CREATE  TABLE category ( 
	id                   integer  DEFAULT nextval('category_id_seq'::regclass) NOT NULL ,
	name                 varchar(50)  NOT NULL ,
	description          varchar(255)   ,
	CONSTRAINT pk_category_id PRIMARY KEY ( id )
 );

CREATE  TABLE movement_type ( 
	id                   integer  DEFAULT nextval('movement_type_id_seq'::regclass) NOT NULL ,
	" name"              varchar(100)  NOT NULL ,
	CONSTRAINT pk_movement_type_id PRIMARY KEY ( id )
 );

CREATE  TABLE role_type ( 
	id                  integer  DEFAULT nextval('role_type_id_seq'::regclass) NOT NULL ,
	name                 varchar(100)  NOT NULL ,
	CONSTRAINT pk_table_id PRIMARY KEY ( id )
 );

CREATE  TABLE settings ( 
	id                  integer  DEFAULT nextval('settings_id_seq'::regclass) NOT NULL ,
	name                 varchar(100)  NOT NULL ,
	"value"              float8  NOT NULL ,
	CONSTRAINT pk_settings_id PRIMARY KEY ( id )
 );

CREATE  TABLE users ( 
	id                  integer  DEFAULT nextval('users_id_seq'::regclass) NOT NULL ,
	firstname            varchar(100)  NOT NULL ,
	lastname             varchar(100)  NOT NULL ,
	email                varchar(100)  NOT NULL ,
	"password"           varchar(250)  NOT NULL ,
	birthdate            date  NOT NULL ,
	phone_number         varchar(20)  NOT NULL ,
	joined_date          date DEFAULT CURRENT_DATE NOT NULL ,
	role_type_id         integer  NOT NULL ,
	CONSTRAINT pk_table_id_0 PRIMARY KEY ( id )
 );

CREATE  TABLE auction ( 
	id                  integer  DEFAULT nextval('auction_id_seq'::regclass) NOT NULL ,
	start_date           timestamp DEFAULT current_timestamp NOT NULL ,
	end_date             timestamp  NOT NULL ,
	title                varchar(100)  NOT NULL ,
	description          text   ,
	category_id          integer  NOT NULL ,
	min_price            float8 DEFAULT 0 NOT NULL ,
	seller_id            integer  NOT NULL ,
	CONSTRAINT pk_auction_id PRIMARY KEY ( id )
 );

CREATE  TABLE auction_operation ( 
	id                   integer  DEFAULT nextval('auction_operation_id_seq'::regclass) NOT NULL ,
	customer_id          integer  NOT NULL ,
	amount               float8  NOT NULL ,
	operation_date       timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL ,
	auction_id           integer  NOT NULL ,
	CONSTRAINT pk_auction_operation_id PRIMARY KEY ( id )
 );

CREATE  TABLE auction_winner ( 
	id                   integer  DEFAULT nextval('auction_winner_id_seq'::regclass) NOT NULL ,
	winner_id            integer  NOT NULL ,
	amount_paid          float8  NOT NULL ,
	"date"               timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL ,
	commission           float8  NOT NULL ,
	auction_id           integer  NOT NULL ,
	administrator_id     integer  NOT NULL ,
	CONSTRAINT pk_auction_winner_id PRIMARY KEY ( id )
 );

CREATE  TABLE token ( 
	id                   integer  DEFAULT nextval('token_id_seq'::regclass) NOT NULL ,
	"value"              varchar(250)  NOT NULL ,
	creation_date        timestamp DEFAULT current_timestamp NOT NULL ,
	expiration_date      timestamp  NOT NULL ,
	users_id             integer  NOT NULL ,
	CONSTRAINT pk_token_id PRIMARY KEY ( id )
 );

CREATE  TABLE account ( 
	id                   integer  DEFAULT nextval('account_id_seq'::regclass) NOT NULL ,
	movement_type        integer  NOT NULL ,
	amount               float8  NOT NULL ,
	date_movement        timestamp DEFAULT CURRENT_TIMESTAMP NOT NULL ,
	administrator_id     integer  NOT NULL ,
	validation           bool DEFAULT false NOT NULL ,
	validation_date      timestamp   ,
	auction_id           integer   ,
	customer_id          integer  NOT NULL ,
	CONSTRAINT pk_account_id PRIMARY KEY ( id )
 );

ALTER TABLE account ADD CONSTRAINT fk_account_auction FOREIGN KEY ( auction_id ) REFERENCES auction( id );

ALTER TABLE account ADD CONSTRAINT fk_account_users FOREIGN KEY ( customer_id ) REFERENCES users( id );

ALTER TABLE account ADD CONSTRAINT fk_account_movement_type FOREIGN KEY ( movement_type ) REFERENCES movement_type( id );

ALTER TABLE auction ADD CONSTRAINT fk_auction_category FOREIGN KEY ( category_id ) REFERENCES category( id );

ALTER TABLE auction ADD CONSTRAINT fk_auction_user FOREIGN KEY ( seller_id ) REFERENCES users( id );

ALTER TABLE auction_operation ADD CONSTRAINT fk_auction_operation_auction FOREIGN KEY ( auction_id ) REFERENCES auction( id );

ALTER TABLE auction_operation ADD CONSTRAINT fk_auction_operation_user FOREIGN KEY ( customer_id ) REFERENCES users( id );

ALTER TABLE auction_winner ADD CONSTRAINT fk_auction_winner_auction FOREIGN KEY ( auction_id ) REFERENCES auction( id );

ALTER TABLE auction_winner ADD CONSTRAINT fk_auction_winner_user_admin FOREIGN KEY ( administrator_id ) REFERENCES users( id );

ALTER TABLE token ADD CONSTRAINT fk_token_users FOREIGN KEY ( users_id ) REFERENCES users( id );

ALTER TABLE users ADD CONSTRAINT fk_users_role_type FOREIGN KEY ( role_type_id ) REFERENCES role_type( id );

INSERT INTO category( id, name, description ) VALUES ( 1, 'Electronic', 'Electronic products like mobile, laptop, etc.' ); 
INSERT INTO category( id, name, description ) VALUES ( 2, 'Clothing', 'Clothing products like shirt, pant, etc.' ); 
INSERT INTO category( id, name, description ) VALUES ( 3, 'Grocery', 'Grocery products like rice, oil, etc.' ); 
INSERT INTO category( id, name, description ) VALUES ( 4, 'Furniture', 'Furniture products like table, chair, etc.' ); 
INSERT INTO category( id, name, description ) VALUES ( 5, 'Book', 'Book products like novel, story, etc.' ); 
INSERT INTO category( id, name, description ) VALUES ( 6, 'Toy', 'Toy products like car, doll, etc.' ); 
INSERT INTO role_type( id, name ) VALUES ( 1, 'Administrator' ); 
INSERT INTO role_type( id, name ) VALUES ( 2, 'Customer' ); 
INSERT INTO settings( id, name, value ) VALUES ( 1, 'Comission', 20.0 ); 
-- admin@gmail.com admin
INSERT INTO users( id, role_type_id, firstname, lastname, email, password, birthdate, phone_number, joined_date ) VALUES ( 1, 1, 'admin', 'admin', '7932b2e116b076a54f452848eaabd5857f61bd957fe8a218faf216f24c9885bb', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1990-01-01', '1234567890', '2023-01-13' ); 
-- user@gmail.com user
INSERT INTO users( id, role_type_id, firstname, lastname, email, password, birthdate, phone_number, joined_date ) VALUES ( 2, 2, 'user', 'user', '02ee7bdc4ccf5c94808a0118eb531822f13e7e38e3810ab29ebefb2c2feb8e58', '04f8996da763b7a969b1028ee3007569eaf3a635486ddab211d512c85b9df8fb', '1990-01-01', '1234567890', '2023-01-13' ); 
CREATE OR REPLACE VIEW v_valid_token as
SELECT t.*, u.role_type_id FROM token t, users u
  WHERE t.users_id=u.id and 
  (EXTRACT(epoch FROM ((t.expiration_date)::timestamp with time zone - now())) > (0)::numeric);


CREATE OR REPLACE VIEW v_valid_token_admin as
SELECT id, value, creation_date,expiration_date, users_id from v_valid_token WHERE  role_type_id=1;


CREATE OR REPLACE VIEW v_valid_token_users as
SELECT id, value, creation_date,expiration_date, users_id from v_valid_token WHERE  role_type_id=2;